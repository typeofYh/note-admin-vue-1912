import knowledge from './knowledge/index.vue';
import article from './article.vue';
import articleAll from './article/all.vue';
import articleCategory from './article/category.vue';
import articleTags from './article/tags.vue';
import user from './user/index.vue';
import setting from './setting/index.vue';
// 所有视图的出口

export default {
  article,
  knowledge,
  articleAll,
  articleCategory,
  articleTags,
  user,
  setting,
};
