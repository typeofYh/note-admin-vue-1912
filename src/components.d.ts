// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/vue-next/pull/3399

declare module 'vue' {
  export interface GlobalComponents {
    AButton: typeof import('ant-design-vue/es')['Button']
    ADropdown: typeof import('ant-design-vue/es')['Dropdown']
    AForm: typeof import('ant-design-vue/es')['Form']
    AFormItem: typeof import('ant-design-vue/es')['FormItem']
    AImage: typeof import('ant-design-vue/es')['Image']
    AInput: typeof import('ant-design-vue/es')['Input']
    AMenu: typeof import('ant-design-vue/es')['Menu']
    AMenuItem: typeof import('ant-design-vue/es')['MenuItem']
    AResult: typeof import('ant-design-vue/es')['Result']
    ASpace: typeof import('ant-design-vue/es')['Space']
    ATable: typeof import('ant-design-vue/es')['Table']
    ATabPane: typeof import('ant-design-vue/es')['TabPane']
    ATabs: typeof import('ant-design-vue/es')['Tabs']
    ChartCom: typeof import('./components/chartCom/index.vue')['default']
    RightContent: typeof import('./components/RightContent/RightContent.vue')['default']
  }
}

export { }
