import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router';
import BasicLayout from '../layouts/BasicLayout.vue';
import authList from '@/auth/user.js';
import notFoundVue from '../views/notFound.vue';
import viewConfig from '@/views/view.config';

const hasGithubPages = import.meta.env.VITE_GHPAGES;
const baseRouter = [
  {
    path: '/worker',
    name: 'worker',
    meta: { title: '工作台', icon: 'icon-icon-test' },
    component: () => import('@/views/worker/WorkerPage.vue'),
  },
];
const router = createRouter({
  history: hasGithubPages ? createWebHashHistory() : createWebHistory(),
  routes: [
    {
      path: '/login',
      name: 'login',
      meta: { title: '登录', showMenuItem: false },
      component: () => import('@/views/login/LoginView.vue'),
    },
    {
      path: '/',
      name: 'index',
      meta: { title: 'Home', showMenuItem: false },
      component: BasicLayout,
      redirect: '/worker',
      children: [
        ...baseRouter,
        {
          path: '/404',
          name: '404',
          meta: { title: '页面找不到', icon: 'icon-icon-test', showMenuItem: false },
          component: () => import('@/views/notFound.vue'),
        },
      ],
    },
  ],
});
// 格式化name
const formatName = (val: string) =>
  val
    .split('/')
    .filter(v => v)
    .map((item, i) => (i === 0 ? item : item[0].toUpperCase() + item.slice(1)))
    .join('');
// 生成侧边菜单数据结构
const menuItemChildren = arr =>
  arr.map(({ path, title, children }) => {
    const name = formatName(path);
    const item = {
      path,
      name,
      meta: {
        title,
        icon: 'icon-icon-test',
      },
      component: viewConfig[name] ? viewConfig[name] : notFoundVue,
    };
    children && Array.isArray(children) && (item.children = menuItemChildren(children));
    return item;
  });

const wihleList = ['/login', '/registry'];

export const getCurUserRole = () => {
  // 得到当前用户视图权限列表
  if (window.location.pathname === '/login') {
    return [];
  }
  // 1. 取当前用户role
  const role = JSON.parse(window.localStorage.getItem('userInfo'))?.role;
  // 用户没有登录
  if (!role) {
    router.replace('/login');
    return [];
  }
  // 当前用户的权限
  const curUserRole = authList.find(val => val.role === role);
  // 当前用户的视图权限
  const viewsRole = menuItemChildren(curUserRole?.viewList);
  console.log('viewsRole:---------', viewsRole);
  return viewsRole;
};

// 导航数据
export const addRoutes = routes => {
  routes.forEach(item => {
    router.addRoute('index', item); // 添加到路由表
  });
};

export const getBaseRoutes = () => {
  return baseRouter;
};

addRoutes(getCurUserRole()); // 添加路由视图

// router.beforeEach((to, from, next) => {
//   if (wihleList.includes(to.path)) {
//     next();
//     return;
//   }
//   // 添加用户视图路由
//   addRoutes(getCurUserRole());
//   setTimeout(() => {
//     next();
//   });
// });

export default router;
