import http from '@/utils/httpTool';

export const getArticleList = params => http.get('/api/article', params);

export const getCategoryList = () => http.get('/api/category');
