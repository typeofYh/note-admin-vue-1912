import http from '@/utils/httpTool';

export const getCommentList = params => http.get('/api/comment', params);

export const passComment = (id, data) => http.patch('/api/comment/' + id, data);
