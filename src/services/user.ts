import http from '@/utils/httpTool';

export const login = data => http.post('/api/auth/login', data);

export const getUserList = params => http.get('/api/user', params);
